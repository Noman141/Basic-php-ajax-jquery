<?php include 'inc/header.php'; ?>
<h2>Topics: Create Show Password Button</h2>
<div class="content">
	<form action="" method="post">
		<table>
			<tr>
				<td>User-Name</td>
				<td>:</td>
				<td>
					<input type="text" name="username" id="username" placeholder="Enter Username">
				</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>:</td>
				<td>
					<input type="password" name="password" id="password" placeholder="Enter Password">
				</td>
			</tr>

			<tr>
				<td></td>
				<td></td>
				<td>
					<button type="button" name="showpassword" id="showpassword" >Show Password</button>
				</td>
			</tr>
			
		</table>
        
	</form>
	
</div>
<?php include 'inc/footer.php'; ?>