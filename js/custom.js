$(document).ready(function(){ 
   // Ajax jquery code for Check User name Availabliity start
   $("#username").blur(function(){
   	 var username = $(this).val();
   	   $.ajax({
   	   	url:("check/check.php"),
   	   	method:"POST",
   	   	data:{username:username},
   	   	dataType:"text",
   	   	success:function(data){
           $('#userstatus').html(data);
   	   	}
   	   });
   });
   // Ajax jquery code for Check User name Availabliity end

   // code for auto compelete box start
   $("#skill").keyup(function(){
      var skill = $(this).val();
      if (skill != '') {
          $.ajax({
            url:("check/checkskill.php"),
            method:"POST",
            data:{skill:skill},
            success:function(data){
           $('#statusskill').fadeIn();
           $('#statusskill').html(data);
            }
         });
      };
   });

   $(document).on('click','li',function(){
     $('#skill').val($(this).text());
     $('#statusskill').fadeOut();
   });

   // code for auto compelete box end

   // code for show password starts
     $("#showpassword").on('click',function(){
     var pass = $("#password");
     var fieldtype = pass.attr('type');
     if (fieldtype == 'password') {
      pass.attr('type', 'text');
      $(this).text("Hide password");
     }else{
       pass.attr('type', 'password');
       $(this).text("Show password");
     }
   });
   // code for show password ends

   // code for Auto Refresh Div Content starts
   $("#autosubmit").click(function(){
    var content = $("#body").val();
    if ($.trim(content) != '') {
        $.ajax({
          url:("check/checkrefresh.php"),
          method:"POST",
          data:{body:content},
          dataType:"text",
          success:function(data){
             $("#body").val("");
          }
         });
        return false;
      }
   });

   setInterval(function(){
    $("#autostatus").load("check/getrefresh.php").fadeIn("slow");
   },1000);
   // code for Auto Refresh Div Content ends

  // code for Live Data Search starts

  $("#livesearch").keyup(function(){
     var live = $(this).val();
     if (live != '') {
          $.ajax({
            url:("check/checklive.php"),
            method:"POST",
            data:{search:live},
            dataType:"text",
            success:function(data){
           $('#statuslive').html(data);
            }
         });
      }else{
        $('#statuslive').html("");
      }
  });
  // code for Live Data Search ends

  // code for Auto Save Data starts
     function autoSave(){
      var content = $("#content").val();
      var contentid = $("#contentid").val();
      if (content != '') {
          $.ajax({
            url:("check/checkautosave.php"),
            method:"POST",
            data:{content:content,contentid:contentid},
            dataType:"text",
            success:function(data){
            if (data != '') {
              $('#contentid').val(data);
             }
             $('#statussave').html("Content Save as Draft");
             setInterval(function(){
              $('#statussave').html("");
             },2000);
            }
         });
          return false;
      }
     }
     setInterval(function(){
        autoSave()
       },10000);
     
  // code for Auto Save Data ends
 });  
